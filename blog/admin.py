from django.contrib import admin
from .models import Post

# регистрация модели на странице администрирования
admin.site.register(Post)
